import sys, os
sys.path.insert(1, os.path.join(sys.path[0], '..'))
import bisect
import os.path
import time
import numpy
from tools.bitarray import bitarray
from tools.tree import AbstractTree
from tools.Zzordercurve_bitarray import Zzcurve


def get_index(key_to_insert,keys):
    i = 0
    for key in keys :
        if key_to_insert < key :
            return i
        i+=1
    return i

class Node:
    def __init__(self, filename=None):
        if filename:
            self.read_data_from_file(filename)

    def read_data_from_file(self, filename):
        global filecounter
        filepath = 'data/'+filename
        lines = [line.strip() for line in open(filepath)]
        self.keys = [bitarray(key) for key in lines[0].split(',')]
        self.children = [child.strip() for child in lines[1].split(',')]
        if lines[2] == 'True':
            self.is_leaf = True
        else:
            self.is_leaf = False
        self.filename = filename
        if self.is_leaf and len(lines) >= 4:
            self.next = lines[3].strip()
        else:
            self.next = None

    def write_data_to_file(self, filename):
        filepath = 'data/' + filename
        with open(filepath, 'w') as f:
            s = ""
            for i in self.keys:
                s += i.to01()+","

            s = s[:-1]  # remove last ,
            f.write(s)
            f.write('\n')
            f.write(str(self.children).strip('[]').replace("'",""))
            f.write('\n')
            f.write(str(self.is_leaf))
            f.write('\n')
            if self.is_leaf and self.next:
                f.write(str(self.next))
                f.write('\n')

    def printContent(self):
        print (self.keys)
        print (self.children)
        print (self.is_leaf)
        print (self.filename)
        if self.is_leaf:
            print (self.next)
        else:
            print('None')

    def updateNode(self):
        self.write_data_to_file(self.filename)

    def splitNode(self):
        global filecounter
        newNode = Node()
        newNode.filename = str(filecounter)
        filecounter = filecounter+1
        if self.is_leaf:
            newNode.is_leaf = True
            mid = len(self.keys)//2
            midKey = self.keys[mid]
            # Update sibling parameters
            newNode.keys = self.keys[mid:]
            newNode.children = self.children[mid:]
            # Update node parameters
            self.keys = self.keys[:mid]
            self.children = self.children[:mid]
            # Update next node pointers
            newNode.next = self.next
            self.next = newNode.filename
        else:
            newNode.is_leaf = False
            mid = len(self.keys)//2
            midKey = self.keys[mid]
            # Update sibling parameters
            newNode.keys = self.keys[mid+1:]
            newNode.children = self.children[mid+1:]
            # Update node parameters
            self.keys = self.keys[:mid]
            self.children = self.children[:mid + 1]
        self.updateNode()
        newNode.updateNode()
        return midKey, newNode

class BPlusTree(AbstractTree):
    def __init__(self, zzcurve , factor, rootfile=-1):
        self.zzcurve = zzcurve
        self.zzcurve = zzcurve
        global filecounter
        self.factor = factor
        if os.path.isfile('.bplustree'):
            filepath = '.bplustree'
            lines = [line.strip() for line in open(filepath)]
            self.root = Node(lines[0].strip())
            filecounter = int(lines[1].strip())
        else:
            self.root = Node()
            # Initialize root
            self.root.is_leaf = True
            self.root.keys = []
            self.root.children = []
            self.root.next = None
            self.root.filename = str(filecounter)
            filecounter += 1
            self.root.updateNode()
            save_tree(self.root.filename, filecounter)

        self.not_in_query_box_counter = 0



    def search(self, key):
        return self.tree_search(key, self.root)

    def tree_search(self, key, node):
        if node.is_leaf:
            return node
        else:
            if key < node.keys[0]:
                return self.tree_search(key, Node(node.children[0]))
            for i in range(len(node.keys)-1):
                if key>=node.keys[i] and key<node.keys[i+1]:
                    return self.tree_search(key, Node(node.children[i+1]))
            if key >= node.keys[-1]:
                return self.tree_search(key, Node(node.children[-1]))



    def insert(self, key, value):
        ans, newFilename =  self.tree_insert_binary(key, value, self.root)
        if ans: #we have to split the root and determine the new root
            global filecounter
            newRoot = Node()
            newRoot.is_leaf = False
            newRoot.filename = str(filecounter)
            filecounter += 1
            newRoot.keys = [ans]
            newRoot.children = [self.root.filename, newFilename]
            newRoot.updateNode()
            self.root = newRoot
        save_tree(self.root.filename, filecounter)

    def tree_insert_binary(self, key, value, node):
        if node.is_leaf:
            index=get_index(key,node.keys)
            node.keys[index:index]=[key]
            filename = self.create_data_file(value)
            node.children[index:index] = [filename]
            node.updateNode()
            if len(node.keys) <= self.factor-1:
                return None, None
            else:
                midKey, newNode = node.splitNode() #changer splitNode function
                return midKey, newNode.filename
        else:
            #si la clé est inférieur au noeud
            if key < node.keys[0]:
                ans, newFilename = self.tree_insert_binary(key, value, Node(node.children[0]))
            #si la clé est supérieur au noeud
            if key >= node.keys[-1] :
                ans, newFilename = self.tree_insert_binary(key, value, Node(node.children[-1]))
            #si la clé est dans le noeud
            for i in range(len(node.keys)-1): #on parcours les clés
                if key >= node.keys[i] and key < node.keys[i+1] :
                    ans, newFilename = self.tree_insert_binary(key, value, Node(node.children[i+1]))

        if ans:
            index=get_index(key,node.keys)
            node.keys[index:index]=[ans]
            node.children[index+1:index+1] = [newFilename]
            if len(node.keys) <= self.factor-1:
                node.updateNode()
                return None, None
            else:
                midKey, newNode = node.splitNode()
                return midKey, newNode.filename
        else:
            return None, None

    def tree_search_for_query(self, key, node):
        if node.is_leaf:
            return node
        else:
            if key <= node.keys[0] :
                return self.tree_search_for_query(key, Node(node.children[0]))
            for i in range(len(node.keys)-1):
                if key>=node.keys[i] and key<node.keys[i+1]:
                    return self.tree_search_for_query(key, Node(node.children[i+1]))
            if key > node.keys[-1]:
                return self.tree_search_for_query(key, Node(node.children[-1]))

    def range_query_(self, keyMin, keyMax):
        all_keys = []
        all_values = []
        start_leaf = self.tree_search_for_query(keyMin, self.root)
        keys, values, next_node = self.get_data_in_key_range(keyMin, keyMax, start_leaf)
        all_keys += keys
        all_values += values
        while next_node:
            keys, values, next_node = self.get_data_in_key_range(keyMin, keyMax, Node(next_node.filename))
            all_keys += keys
            all_values += values
        return all_keys, all_values

    def in_query_box( self, keyMin, keyMax , node ):
        z_keyMin = self.zzcurve.reverse_z_order( keyMin , \
                self.zzcurve.first_dimention ,
                self.zzcurve.attribute_length )
        z_keyMax = self.zzcurve.reverse_z_order( keyMax , \
                self.zzcurve.first_dimention ,
                self.zzcurve.attribute_length )

        z_node = self.zzcurve.reverse_z_order( node , \
                self.zzcurve.first_dimention ,
                self.zzcurve.attribute_length )

        for m,M,n in zip(z_keyMin , z_keyMax , z_node):
            if n < m or M < n:
                return False
        return True



    def get_data_in_key_range(self, keyMin, keyMax, node):
        keys = []
        values = []
        for i in range(len(node.keys)):
            key = node.keys[i]
            print("Keymin",keyMin,"key",key,"keymax",keyMax)
            print(keyMin <= key, key <= keyMax )
            if keyMin <= key  and key <= keyMax :
                if self.in_query_box(keyMin,keyMax,key):
                    print("key :",key)
                    keys.append(key)
                    values.append(self.read_data_file(node.children[i]))
                else:
                    self.not_in_query_box_counter+=1
        if node.keys[-1] > keyMax:
            next_node = None
        else:
            if node.next:
                next_node = Node(node.next)
            else:
                next_node = None
        return keys, values, next_node

    def range_query(self, _range):
        print("range",_range[0]," ",_range[1])
        return self.range_query_(_range[0],_range[1])



    def print_space(self,space):
        for i in range(space):
            print(" ",end='')

    def print_tree(self,node=-42,space=0):
        if node == -42:
            node=self.root
        space+=1
        if node.is_leaf:
            for i in range(len(node.keys)):
                self.print_space(space)
                print(node.keys[i]),
        else:
            for i in range(len(node.keys)):
                self.print_space(space)
                print(node.keys[i])
            for i in range(len(node.keys)+1):
                if node.children[i]!=None :
                    self.print_tree(Node(node.children[i]),space)
                self.print_space(space)
                print("")

    def __str__(self, __node=-42, __i_level = 0):
        # I really love ugly and useless code <3
        # TODO : do a functionnal str
        indent ="    "
        output=""
        if __node==-42:
            __node = self.root
            print(self.root)

        for key in __node.keys:
            output += indent*__i_level +str(key) + "\n"

        if __node.is_leaf:
            return output+"\n"
        else:
            for child  in [ Node(c) for c in __node.children]:
                output += self.__str__( child , __i_level + 1 )
        return output+"\n"

    def __repr__(self):
        return self.__str__()


    def create_data_file(self, value):
        global filecounter
        filename = str(filecounter)
        filepath = 'data/'+filename
        with open(filepath, 'w') as f:
            f.write(str(value))
        filecounter += 1
        return filename

    def read_data_file(self, filename):
        filepath = 'data/'+filename
        lines = [line.strip() for line in open(filepath)]
        return lines[0].strip()

def save_tree(root, filecounter):
    filepath = '.bplustree'
    with open(filepath, 'w') as f:
        f.write(root)
        f.write('\n')
        f.write(str(filecounter))
        f.write('\n')

filecounter = 0         # Used to keep track of filename
if __name__ == '__main__':
    # Initialize variables
    start_time = 0          # Used to store start time
    end_time = 0            # Used to store end time
    insert_time = []
    search_time = []
    range_time = []
    insert_disk = []
    search_disk = []
    range_disk = []
    # Load Configuration
    configs = [line.strip() for line in open('bplustree.config')]
    max_num_keys = int(configs[0].strip())
    factor = max_num_keys-1

    # Do not initialize the tree.. Load from .bplustree
    if os.path.isfile('.bplustree'):
        filepath = '.bplustree'
        lines = [line.strip() for line in open(filepath)]
        root = lines[0].strip()
        tree = BPlusTree(factor, root)
        filecounter = int(lines[1].strip())
    # Initialize the tree
    else:
        tree = BPlusTree(factor)

    # Perform insert operations
    if sys.argv[1] == 'insert':
        print('Inserting Data')
        if len(sys.argv) >= 3:
            filepath = sys.argv[2]
        else:
            filepath = 'assgn2_bplus_data.txt'
        lines = [line.strip() for line in open(filepath)]
        for line in lines:
            line = line.split()
            keyb=bitarray(line[0].strip())
            print(keyb)
            key = float(line[0].strip())
            value = line[1].strip()
            #tree.insert(key, value)
            tree.insert(keyb,value)
        print('Insertions successfully completed')
        tree.print_tree(tree.root,0)

    # Perform query operations
    if sys.argv[1] == 'query':
        print('Running queries')
        if len(sys.argv) >= 3:
            filepath = sys.argv[2]
        else:
            filepath = 'querysample.txt'
        # Query
        lines = [line.strip() for line in open(filepath)]
        for line in lines:
            line = line.split()
            operation = int(line[0].strip())
            # Range Query
            if operation == 2:
                keyMin = bitarray(line[1].strip())
                keyMax = bitarray(line[2].strip())
                _range = [keyMin,keyMax]
                keys, values = tree.range_query(_range)
                if len(values) > 0:
                    zipped = zip(keys, values)
                    for i in zipped:
                        print(i)
                else:
                    print('Not Found')

    # Save tree configuration
    save_tree(tree.root.filename, filecounter)
