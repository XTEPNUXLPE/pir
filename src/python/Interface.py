
import configparser, os, sys, pathlib , time
from tools.Zzordercurve_bitarray import Zzcurve
from tools.bitarray import bitarray


def usage( config , argv0 ):
    print("%s [command] [tree type]" % argv0 )
    print("Commands : usage insert range_query")
    trees = ""
    for i in config.sections():
        if not i == 'general':
            trees+=" "+i

    print("Tree types : %s" % trees)
    return

def insertions(data , min_max_dimention_file , interface ):
    z_indexess=[]
    times = []
    d = data.readlines()
    for line in d:
        key,value=line.split("|")
        sequence = [[bitarray(attribute) for attribute in element.split()] \
                for element in key.split(',') ]
        begin = time.time()
        a = interface.insert(sequence,value)
        end = time.time()
        times.append(end-begin)
        z_indexess.append(interface.insert(sequence,value))


    for i in range(len(z_indexess[0])):
        val = [ z_indexes[i] for z_indexes in z_indexess ]

        min_max_dimention_file.write(min(val).to01())
        min_max_dimention_file.write("\n")
        min_max_dimention_file.write(max(val).to01())
        min_max_dimention_file.write("\n")

    return times


def range_queries( dim ,qf , interface ):
    times = []
    m=[]
    M=[]
    _range_zorders=[[],[]]
    dimentions = [bitarray(i[:-1]) for i in dim]
    for i in map(lambda a:a*2 , range(len(dimentions)//2)):
        m.append(dimentions[i])
        M.append(dimentions[i+1])


    for line in qf:
        _range_zorders=[[],[]]
        sequence = [[bitarray(attribute) for attribute in element.split()] \
                for element in line.split(',') ]
        for i in range(interface.first_dimention):
            if i < len(sequence):
                _range_zorders[0].append(interface.zzcurve.z_order(sequence[i], \
                        interface.attribute_length ) )

                _range_zorders[1].append(interface.zzcurve.z_order(sequence[i], \
                        interface.attribute_length ))
            else:
                _range_zorders[0].append(m[i])
                _range_zorders[1].append(M[i])
        _range = [ interface.zzcurve.z_order(i, interface.attribute_length * \
                interface.second_dimention )  for i in _range_zorders]


        begin = time.time()
        interface.range_query(_range)
        end = time.time()
        times.append(end-begin)

        n = len(times)
    return n, sum(times)/n



# TODO : class completely useless : integrate range_queries and  insertions in
# it
class Interface:
    def __init__(self,attribute_length,first_dimention,second_dimention,zzcurve,tree):
        self.first_dimention = first_dimention
        self.second_dimention = second_dimention
        self.attribute_length = attribute_length
        self.zzcurve = zzcurve
        self.tree=tree

    def insert(self, sequence , value):
        z_indexes,z_index = self.zzcurve.zz_order(sequence)
        self.tree.insert(z_index , value)
        print("inserted :", self.zzcurve.reverse_z_order( \
                z_index,self.first_dimention , self.attribute_length*
                self.second_dimention))
        return z_indexes # on retourne les z_indices d'ordre 2 pour le min/max

    def range_query(self, _range):
        output = self.tree.range_query(_range)
        #TODO: insert range query extension algorithm
        return output

# TODO : it's horrible : only use tree class and not an ugly think like this
# TODO : remove duplicatie zzcurve in objects
class Tree:
    def __init__(self, tree_class, zzcurve_ , options= "" ):
        o = ",".join( \
                [ option[0]+"="+option[1] \
                for option in map( \
                    lambda a:a.split(" ") ,\
                    filter( lambda a: a!="" , options.split("\n"))\
                    )\
                ])
        print(o)
        print(zzcurve_)
        print("self.tree="+tree_class+"( zzcurve,"+o+")")
        exec("self.tree="+tree_class+"( zzcurve,"+o+")")
        self.insert = self.tree.insert
        self.range_query = self.tree.range_query




if __name__ == "__main__":
    try:
        config = configparser.ConfigParser()
        config.read('interface.conf')

        if sys.argv[1] == 'usage':
            usage(config,sys.argv[0])
            sys.exit()

        if sys.argv[2]== 'general' or sys.argv[2] not in config.sections():
            usage(config,sys.argv[0])
            raise ValueError("Tree type doesn't exist")

        tree_conf = sys.argv[2]

        # take global configuration
        al = int(config['general']['attribute_length'])
        fd = int(config['general']['first_dimention'])
        sd = int(config['general']['second_dimention'])
        data_file = config['general']['data_file']
        query_file = config['general']['query_file']
        mm_file = config['general']['min_max_dimention_file']

        logfile = config['general']['log_folder'] + "/"
        logfile += tree_conf + "_" + sys.argv[1] + ".csv"
        zzcurve = Zzcurve(al, fd,sd)
        # take tree configuration

        location = config[tree_conf]['location']
        folder = config[tree_conf]['data_directory']
        tree_class = config[tree_conf]['tree_class']
        if 'options' in config[tree_conf].keys():
            options = config[tree_conf]['options']



        if sys.argv[1] == 'insert':
            with open(data_file,'r') as data, open( mm_file ,'w' )as dim , \
                    open(logfile,'a') as log :
                exec("from %s import %s" % ( location , tree_class))
                os.chdir(folder)
                print("wd : ",os.getcwd())
                tree = Tree( tree_class , zzcurve , options )
                interface = Interface( al, fd , sd, zzcurve,tree )
                times = insertions( data, dim , interface)
                n = len(times)
                log.write(str(n)+","+str(sum(times)/n)+"\n")

        elif sys.argv[1] == 'range_query':
            with open( mm_file,'r') as dim,\
                    open(logfile,'a') as log, \
                    open(query_file,'r') as qf:
                # logfile preparation
                log.write(str(al)+","+str(fd)+","+str(sd)+",")

                # execution
                exec("from %s import %s" % ( location , tree_class ))
                os.chdir(folder)
                print("wd : ",os.getcwd())
                tree = Tree( tree_class, zzcurve , options )
                interface = Interface( al , fd , sd , zzcurve, tree )
                n, average = range_queries( dim , qf , interface )
                log.write(str(n)+","+str(average)+"\n")

        else:
            usage()

    except ValueError as e:
        print(e,file=sys.stderr)
