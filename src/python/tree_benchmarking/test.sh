
for (( i=1; i<=$1; i++ ))
do
    python generate_data.py data $(($i*10)) 10 10 100;
    python bptree/test.py $(($i*10));
    python kdtree/test.py $(($i*10));
done
