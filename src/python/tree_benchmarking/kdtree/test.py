import sys, os , time
sys.path.insert(1, os.path.join(sys.path[0], '../..'))
from tools.bitarray import bitarray
from tools.Zzordercurve_bitarray import Zzcurve
import kdtree

with open("data_in.data","r") as data,open("results_kdtree","a") as results:
    fd = int(sys.argv[1])
    sd = 10
    zzcurve = Zzcurve( 10, fd , sd)
    tree = kdtree.create(dimensions=fd)
    times = []

    for line in data:
        key,value=line.split("|")
        sequence = [[bitarray(attribute) for attribute in element.split()] \
                for element in key.split(',') ]

        begin = time.time()
        to_insert = [ zzcurve.z_order(element,fd*10) for element in sequence ]
        tree.add(sequence)
        end = time.time()
        times.append(end - begin)

    n = len(times)
    results.write(str(fd)+","+str(sd)+","+str(n)+","+str(sum(times)/n)+"\n")


