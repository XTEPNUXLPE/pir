import sys, random

def usage(ar):
    print("%s data [sequence length] [element length] [attribute length] [number of data]\n" % ar )
    print("%s query [ max sequence length] [element length] [attribute length] [number of query]\n" % ar )

def random_bitarray_str(attribute_lentgh):
    output = ""
    for i in range(attribute_lentgh):
        output += str(random.randrange(2))
    return output


def random_data_line( sequence_length , element_length , attribute_length):
    output = ""
    sequence = []
    for i in range(sequence_length):
        element = []
        for i in range( element_length ):
            element.append( random_bitarray_str(attribute_length) )
        sequence.append(" ".join(element))

    output += ",".join(sequence) + "|" + "nothing" + "\n"

    return output


def random_query_line( sequence_length , element_length , attribute_length):
    output = ""
    sequence = []
    n = random.randrange(1,sequence_length)
    for i in range(n):
        element = []
        for i in range( element_length ):
            element.append( random_bitarray_str(attribute_length) )
        sequence.append(" ".join(element))

    output += ",".join(sequence) + "\n"

    return output



if __name__ == "__main__" :
    if len(sys.argv) < 6:
        usage(sys.argv[0])

    sequence_length = int(sys.argv[2])
    element_length = int(sys.argv[3])
    attribute_length = int(sys.argv[4])
    n = int(sys.argv[5])

    if sys.argv[1] == "data":
        with open("data_in.data","w") as d:
            for i in range(n):
                d.write(random_data_line( sequence_length, element_length , \
                        attribute_length ))

    elif sys.argv[1] == "query":
        with open("query.data","w") as d:
            for i in range(n):
                d.write(random_query_line( sequence_length, element_length , \
                        attribute_length ))
    else :
        usage(sys.argv[0])
        exit(0)





