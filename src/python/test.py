from tools.bitarray import bitarray

if __name__=="__main__":
    x = bitarray('101')
    print(x)
    y = bitarray('100')
    print(y)
    print(x==y)


    x = bitarray('1010011101100001')
    print(x)
    y = bitarray('0111111111110101')
    print(y)
    print(x<=y)
    print(y<=x)

