global :
--------
- [ ] global makefile
- [x] define comparison and operations for bitarray ( look if not already implemented )

interface :
-----------
- [x] range query
- [ ] statistics
- [ ] lot of query mode
- [ ] random insertion mode
- [ ] redo all interface with nices structures

BPT :
-----
- [ ] clean useless code
- [ ] redo Node ( store correctly informations and reuse info of existents files )
- [ ] stock really in binary
- [ ] get rid of makefile or in global makefile


Later :
-------
- [ ] tree inheritance ( with method insert and range_query to define )
- [ ] reduce config length ( with inheritance )
