import abc
from abc import abstractmethod

class AbstractTree(abc.ABC):

    @abstractmethod
    def insert(self,pattern,value):
        pass

    @abstractmethod
    def range_query(self, _range):
        pass
