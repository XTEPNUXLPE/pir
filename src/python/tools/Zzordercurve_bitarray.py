from tools.bitarray import bitarray
from copy import deepcopy

class Zzcurve:
    def __init__(self, attribute_length,first_dimention , second_dimention ):
        self.attribute_length = attribute_length
        self.first_dimention = first_dimention
        self.second_dimention = second_dimention

    def z_order(self , element , attribute_length ):
        z_index = bitarray('')

        # put attributes at same lentgh
        for attribute in element:
            for i in range(len(attribute),attribute_length):
                attribute.insert(0,0)

        # z_index calculation
        for i in range(attribute_length):
            for attribute in element:
                z_index.append(attribute[i])

        return z_index


    def zz_order(self, sequence ):
        z_indexes = []
        for element in sequence:
            z_indexes.append(self.z_order( element , self.attribute_length))

        return z_indexes,self.z_order(z_indexes, self.attribute_length * \
                self.second_dimention)

    def reverse_z_order( self, z_index , dimention, attribute_length):
        z_indexes = []
        for i in range(dimention):
            z_indexes.append(bitarray(''))

        for i in range(attribute_length):
            for j in range(dimention):
                z_indexes[j].append(z_index[i*dimention+j])

        return z_indexes


    def reverse_zz_order( self, z_index_1 ):

        z_indexes = []

        z_indexes_1 = self.reverse_z_order( z_index_1 , self.first_dimention ,
                self.second_dimention * self.attribute_length )

        for z_index in z_indexes_1:
            z_indexes_2 = self.reverse_z_order( z_index, self.second_dimention,
                    self.attribute_length)
            z_indexes.append( z_indexes_2 )


        return z_indexes


if __name__ == "__main__":
    l = [ [ bitarray('') , bitarray('1') , bitarray('10') ],[
        bitarray('11') , bitarray('100') , bitarray('101') ],[ bitarray('10')
            , bitarray('001') , bitarray('010') ] ]

    a = Zzcurve(3,3,3)
    test = a.zz_order(l)
    print("first test :",test,"\n\n")

    test2 = a.reverse_zz_order( test )
    print("second test : " ,test2)
    print( l )

