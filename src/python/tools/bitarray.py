import bitarray as b

class bitarray(b.bitarray):
    def extend(self,n):
        for i in range(n):
            self.insert(0,0)

    def __lt__(self,other):
        ns = len(self)
        no = len(other)
        if no > ns:
            n = no
            self.extend(no-ns)
        else :
            n = ns
            other.extend(ns-no)

        for i in range(n):
            if self[i] != other[i]:
                return other[i]
        return False

    def __le__(self,other):
        ns = len(self)
        no = len(other)
        if no > ns:
            n = no
            self.extend(no-ns)
        else :
            n = ns
            other.extend(ns-no)

        for i in range(n):
            if self[i] != other[i]:
                return other[i]
        return True

    def __eq__(self, other):
        ns = len(self)
        no = len(other)
        if no > ns:
            n = no
            self.extend(no-ns)
        else :
            n = ns
            other.extend(ns-no)

        for i in range(n):
            if self[i] != other[i]:
                return False
        return True

    def __ne__(self,other):
        return not self.__eq__(other)

    def __gt__(self,other):
        return not self.__le__(other)

    def __ge__(self,other):
        return not self.__lt__(other)


    def __add__(self,other):
        ns = len(self)
        no = len(other)
        if no > ns:
            n = no
            self.extend(no-ns)
        else :
            n = ns
            other.extend(ns-no)

        r=0
        result = bitarray('')
        for i in reversed(range(n)):
            a = r + self[i] + other[i]
            b = a % 2
            r = a //2
            result.insert(0,b)
        while r:
            result.insert(0,1)
            r-=1

        return result

    def __sub__(self,other):
        ns = len(self)
        no = len(other)
        if no > ns:
            n = no
            self.extend(no-ns)
        else :
            n = ns
            other.extend(ns-no)

        result = bitarray('')
        r = 0
        for i in reversed(range(n)):
            a = r + self[i] - other[i]
            b = a % 2
            r = a//2
            print(r)
            result.insert(0,b)

        if r < 0:
            raise ValueError("Negative value")

        return result

    def __str__(self):
        return str(sum([i*(2**n) for (n,i) in enumerate(reversed(self))])) \
                + " : " + self.to01()

    def __repr__(self):
        return str(sum([i*(2**n) for (n,i) in enumerate(reversed(self))])) \
                + " : " + self.to01()
